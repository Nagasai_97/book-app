import { useState } from "react";
function BookInfo(props){
const [bookName,setBookName] = useState('')
const [authorName,setAuthorName]=useState('')
const onSubmit=(e)=>{
    e.preventDefault()
    if(bookName.length!==0 && authorName!==0){
        props.onBookInfo(bookName,authorName)
    
    }
    else {
        alert('enter the value')
    }
   
}
    return(
        <form className="bookInfo">
            <h1>{props.title}</h1>
                    <p>Book Name:
                    <input onChange={(e)=>{setBookName(e.target.value)}}/></p>
                    <p>Author Name:
                    <input onChange={(e)=>{setAuthorName(e.target.value)}} /></p>
                    <button onClick={onSubmit} className="bookSubmitButton">submit</button>
        </form>
    )
}

export default BookInfo;