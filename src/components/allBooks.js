import React, { useEffect, useState } from 'react';
import SearchBook from './searchBook';
import './style.css';
import {fetchData,fetchDeleteBook,postData,updatedata} from '../utils/fetchData'
import BookInfo from './bookInfo';


function AllBooks() {
const [allBooks,setAllBooks] = useState([])
const [addBook,setAddBook] = useState(false)
const[updateBook,setUpdateBook] = useState(false)
const [updateBookId,setUpdateBookId] = useState()

useEffect(()=>{
    let url = 'https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/get'; 
    async function fetchMyAPI() {
        let res =  await fetchData(url)
        setAllBooks(res)
    }
    fetchMyAPI()
    
},[])
const onSearch=async(e)=>{
    if(e.key === 'Enter'){
    let url = `https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/search/${e.target.value}`
    let res =  await fetchData(url)
        setAllBooks(res)
    }   
}
const deleteBook=async(data)=>{
    let id =parseInt(data);
    let deletUrl=`https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/delete/${id}`;
    await fetchDeleteBook(deletUrl)
    getData()
}
const onAddBook=async(bookName,authorName)=>{
    let url="https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/post";
    await postData(url,bookName,authorName)
    setAddBook(false)
    getData()
}
const onUpadateBook=async(bookName,authorName)=>{
    let url=`https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/put/${updateBookId}`;
    await updatedata(url,bookName,authorName)
    getData()
    setUpdateBook(false)
}
const getData=async()=>{
    alert(1)
    let url = 'https://ok2g4b047c.execute-api.us-east-1.amazonaws.com/get'; 
          let res =  await fetchData(url)
          setAllBooks(res)
}
const onUpdateButton=(e)=>{
    
    setUpdateBook(true)
    setUpdateBookId(parseInt(e.target.id))
}
        return (
            <div>
                <div className="headerSection">
                <SearchBook  onSearch={onSearch}/>
                { addBook === false &&
                <button onClick={()=>{setAddBook(true)}} className="addButton">Add book +</button>
                }
                 </div>
                { addBook &&
                <BookInfo onBookInfo={onAddBook} title="Add Book"/>
                }
             
                    {
                        allBooks.length === 0 &&
                        <span className="loading">Loading...</span>
                    }
                    {
                        updateBook &&
                        <BookInfo onBookInfo={onUpadateBook} title="Update Book"/>
                    }
                
                <div className="totalBooks">
                {allBooks.length > 0 &&
                    allBooks.map((eachBook)=>{
                        return <div className="eachBookDiv" key={eachBook.id}>
                            <h1>{eachBook.bookname}</h1>
                            <p>{eachBook.author}</p>
                            <div className="bookFooter">
                            <p id={eachBook.id} onClick={(e) => deleteBook(e.target.id)} className="deleteIcon">-</p>
                            <p onClick={onUpdateButton} id={eachBook.id}>UpdateBook</p>
                            </div>
                        </div>

                    })
                    
                }
                </div>
            </div>
        );
    
}

export default AllBooks;