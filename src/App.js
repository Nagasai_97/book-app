
import './App.css';
import AllBooks from './components/allBooks';

function App() {
  return (
    <div>
      <AllBooks />
    </div>
  );
}

export default App;
