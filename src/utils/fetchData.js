import axios from "axios"
export const fetchData=(url)=>{
    let responseData = axios.get(url).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return responseData;
}

export const postData=(url,bookName,authorName)=>{
    let responseData = axios.post(url,{bookname:bookName,author:authorName}).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return responseData;
}

export const fetchDeleteBook=(url)=>{
    let deleteData = axios.delete(url).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return deleteData;
}

export const updatedata=(url,bookName,authorName)=>{
    let updateData = axios.put(url,{bookname:bookName,author:authorName}).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return updateData;

}

